## Preparando a máquina para o projeto

- A sua máquina deverá conter o Apache e o MySQL funcionando
- Crie uma pasta dentro do Apache e insira todos os arquivos nela

## Configurando a aplicação

- Ao acessar a pasta pelo navegador (Ex.: localhost/PASTA_CRIADA/) você será redirecionado para a configuração do banco de dados
- Coloque as credencias do banco de dados da sua máquina e um nome para o banco dedados da aplicação

## Usando a aplicação

Na primeira tela haverá dois formulários, um para criar um novo NIS para o nome descrito no campo e outro para buscar NIS cadastrados.

Quando for criar um novo NIS, será direcionado para uma página onde será fornecido o NIS gerado para esta pessoa. Este NIS será armazenado no banco de dados criado anteriormente para futuras consultas. Guarde o NIS informado para consultas futuras.

Quando for buscar um NIS, informe o número do NIS para que seja buscado no banco de dados onde retornará como resultado o nome e o NIS da pessoa que procura.

Projeto desenvolvido por Douglas Gondim.