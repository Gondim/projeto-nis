<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Busca NIS</title>

	<?php 
		include("classes/conexao.class.php");
		include("classes/base.class.php"); 

		$base = new Base();
	?>

	<link rel="stylesheet" href="includes/css/style.css">
	<link rel="stylesheet" href="includes/css/busca.css">
</head>
<body>
	<section id="busca" class="content">
		<a href="<?php echo dirname($_SERVER["REQUEST_URI"]); ?>"><< Voltar</a>

		<div id="message-error2" class="none"></div>
		<form action="" method="get" id="formBusca">
			<div class="field-group text-center">
				<label for="num_nis">Digite o NIS para buscar:</label>
				<input type="number" class="form-field" name="search" id="num_nis" placeholder="Digite o número do NIS" value="<?php echo $nis = ( isset( $_GET['search'] ) ) ? $_GET['search'] : ''; ?>" required >
			</div>

			<button type="submit" class="button">Enviar</button>
		</form>

		<?php
			if( isset( $_GET['search'] ) ) : 
				
				$resultado = $base->buscaNIS( $_GET['search'] );

				if( count($resultado) > 0 ) :
		?>
					<table class="table">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Número do NIS</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								foreach ($resultado as $row) { 
									echo '
									<tr>
										<td>'.$row['nome'].'</td>
										<td>'.$row['nis'].'</td>
									</tr>';
								} 
							?>
						</tbody>
					</table>
				<?php else: ?>
					<p>Cidadão não encontrado.</p>
		<?php
				endif; 
			endif;
		?>
	</section>

	<script src="includes/js/jquery.min.js"></script>
	<script src="includes/js/forms.js"></script>
</body>
</html>