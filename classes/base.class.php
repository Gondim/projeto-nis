<?php
/**
* Base das funções utilizadas no projeto
*/
class Base extends Conexao
{
	public function geradorNIS($arr) 
	{
		$nis = "";
		for($i=0;$i<11;$i++) {
			$numero = mt_rand(0,9);
			$nis .= $numero;
		}
		if( in_array($nis, $arr) ) {
			$nis = $this->geradorNIS($arr);
		} else {
			return $nis;
		}
	}

	public function adicionaPessoa($nome) 
	{
		if( empty( $nome ) ) {
			return false;
		}
		
		$sql = "SELECT nis FROM pessoas_nis";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$arr = array();
		foreach ($rows as $key => $value) {
			array_push($arr, $value['nis']);
		}

		$nis_gerado = $this->geradorNIS($arr);
		
		$nome = addslashes( trim( $nome ) );

		try {
			$sql = "INSERT INTO pessoas_nis (nome, nis) VALUES ('$nome', $nis_gerado)";
			$stmt = $this->conn->prepare($sql);
			$stmt->execute();
		
			return $nis_gerado;

		} catch (PDOException $e) {
			return $e->getMessage();
		}
		
	}

	public function buscaNIS($busca)
	{
		$pesquisa = $this->getNumeros($busca);

		try {
			$sql = "SELECT nome, nis FROM pessoas_nis WHERE nis = '$busca'";
			$stmt = $this->conn->prepare($sql);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

			return $rows;

		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function getNumeros($numero)
    {
        $numero = preg_replace("([^0-9])", "", $numero);
        return $numero;
    }
}
