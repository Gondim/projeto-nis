<!DOCTYPE html>
<html lang="pt-br">
<head>
	<?php
	if( file_exists('classes/conexao.class.php') ) {
		header('Location: '.dirname($_SERVER["REQUEST_URI"]));
	}
	?>
	<meta charset="UTF-8">
	<title>Configurações de conexão</title>

	<link rel="stylesheet" href="includes/css/style.css">
	<link rel="stylesheet" href="includes/css/config.css">
</head>
<body>
	<section id="configuracoes" class="content">
		<form action="<?php echo dirname($_SERVER["REQUEST_URI"]); ?>/creator.php" method="post">
			<p class="text-center">Preencha o formulário para iniciar a aplicação. Os dados serão utilizados para a criação de um banco de dados.</p>
			<div class="field-group">
				<label for="host_tx">Host do banco de dados:</label>
				<input type="text" class="form-field" name="host" id="host_tx">
			</div>
			<div class="field-group">
				<label for="user_tx">Usuário do banco de dados:</label>
				<input type="text" class="form-field" name="user" id="user_tx">
			</div>
			<div class="field-group">
				<label for="pass_tx">Senha do banco de dados:</label>
				<input type="text" class="form-field" name="pass" id="pass_tx">
			</div>
			<div class="field-group">
				<label for="banco_tx">Nome para o banco de dados:</label>
				<input type="text" class="form-field" name="banco" id="banco_tx">
			</div>
			<button type="submit" class="button">Enviar</button>
		</form>
	</section>
</body>
</html>