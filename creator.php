<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Configurando banco de dados</title>
	<link rel="stylesheet" href="includes/css/style.css">
</head>
<body>
	<?php
		if( !isset( $_POST['host'] ) || !isset( $_POST['user'] ) || !isset( $_POST['pass'] ) || !isset( $_POST['banco'] ) ) :

			echo '
			<div class="content">
				<p>Desculpe! Nenhuma informação foi recebida</p>

				<a href="<?php echo $_SERVER["REQUEST_URI"]; ?>"><< Voltar</a>
			</div>';

		else :

			$host = $_POST['host'];
			$user = $_POST['user'];
			$pass = $_POST['pass'];
			$bd   = $_POST['banco'];

			try {
				$conn = new PDO("mysql:host={$host}", $user, $pass);
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql = "CREATE DATABASE IF NOT EXISTS {$bd}";
    			$conn->exec($sql);
    			$sql = "use {$bd}";
    			$conn->exec($sql);
    			$sql = "CREATE TABLE `pessoas_nis` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `nome` text DEFAULT NULL,
						  `nis` text DEFAULT NULL,
						  PRIMARY KEY (`id`)
						) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
		    	$conn->exec($sql);

			} catch (PDOException $e) {
				echo '
					<div class="content">
						<p>Erro: '.$e->getMessage().'</p>

						<a href="<?php echo $_SERVER["REQUEST_URI"]; ?>"><< Voltar</a>
					</div>';
				exit();
			}

			$conteudo = 
'<?php
/**
* Classe para conexão
*/
class Conexao
{
	private $driver = \'mysql\';
    private $host   = \''.$_POST['host'].'\';
    private $db     = \''.$_POST['banco'].'\';
    private $user   = \''.$_POST['user'].'\';
    private $pass   = \''.$_POST['pass'].'\';

    protected $conn;

	function __construct()
	{
		try {
	        $this->conn = new PDO("{$this->driver}:host={$this->host}; dbname={$this->db}", $this->user, $this->pass);
	        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            throw new Exception("Erro na conexão com o banco", 1);
            exit();
        }
	}
}';

			$arquivo = fopen('classes/conexao.class.php', 'w');
			fwrite($arquivo, $conteudo);
			fclose($arquivo);

			if( !file_exists( 'classes/conexao.class.php' ) ) {
				echo '
					<div class="content">
						<p>Houve um erro ao criar arquivo de configuração!</p>

						<a href="'.dirname($_SERVER["REQUEST_URI"]).'"><< Voltar</a>
					</div>';
				exit();
			} else {
				header( 'Location: '.dirname($_SERVER["REQUEST_URI"]) );
			}

		endif;
	?>
</body>
</html>
