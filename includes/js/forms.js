jQuery(function($){

	$("#formCadastro").submit(function(){

		var nome = $("#name");
		if( nome.val().trim() == "" ) {
			$("#message-error1").html("Por favor, insira o nome!");
			$("#message-error1").removeClass('none');
			return false;
		} else {
			$("#message-error1").html("");
			$("#message-error1").addClass('none');
			return true;
		}

	});

	$("#formBusca").submit(function(){

		var nis = $("#num_nis");
		if( nis.val().trim() == "" ) {
			$("#message-error2").html("Por favor, insira o NIS!");
			$("#message-error2").removeClass('none');
			return false;
		} else if( nis.val().replace(/[^0-9]/g, '').length != 11 ) {
			$("#message-error2").html("Por favor, insira os 11 dígitos do NIS!");
			$("#message-error2").removeClass('none');
			return false;
		} else {
			$("#message-error2").html("");
			$("#message-error2").addClass('none');
			return true;
		}

	});

});