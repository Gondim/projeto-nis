<?php 
	if( !file_exists('classes/conexao.class.php') ) {
		header('Location: '.$_SERVER["REQUEST_URI"].'config.php');
	}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Cadastro de NIS</title>

	<link rel="stylesheet" href="includes/css/style.css">
</head>
<body>
	<section id="cadastro" class="content">
		<h1 class="page-title">Desafio - Desenvolvedor</h1>
		<div class="linha">
			<article class="coluna">
				<div class="border">
					<p class="text-center">Preencha o campo abaixo para gerar um NIS</p>
				
					<div id="message-error1" class="none"></div>
					<form action="<?php echo $_SERVER["REQUEST_URI"]; ?>obrigado.php" method="post" class="forms" id="formCadastro">
						<div class="field-group text-center">
							<label for="name">Nome completo:<span class="field-required">*</span></label>
							<input type="text" name="nome" id="name" class="form-field" required>
						</div>
						
						<button type="submit" class="button">Enviar</button>
					</form>
				</div>
			</article>

			<article class="coluna">
				<div class="border">
					<p class="text-center">Busque NIS cadastrados</p>

					<div id="message-error2" class="none"></div>
					<form action="<?php echo $_SERVER["REQUEST_URI"]; ?>busca.php" method="get" class="forms" id="formBusca">
						<div class="field-group text-center">
							<label for="num_nis">Número do NIS:<span class="field-required">*</span></label>
							<input type="number" name="search" id="num_nis" class="form-field" required>
						</div>

						<button type="submit" class="button">Buscar</button>
					</form>
				</div>
			</article>
		</div>
		
	</section>

	<script src="includes/js/jquery.min.js"></script>
	<script src="includes/js/forms.js"></script>
</body>
</html>