<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Obrigado</title>

	<?php 
		include("classes/conexao.class.php");
		include("classes/base.class.php"); 
	?>

	<link rel="stylesheet" href="includes/css/style.css">
</head>
<body>
	<?php
		if(empty($_POST['nome'])) {
			header( 'Location: '.dirname($_SERVER["REQUEST_URI"]) );
		}

		$nome = $_POST['nome'];
		$base = new Base;
		$nis = $base->adicionaPessoa($nome);
	?>
	<section id="obrigado" class="content">
		<h1 class="page-title">Olá, <?php echo $nome; ?>!</h1>

		<div class="message">
			<p>Seu NIS é: <?php echo $nis; ?> </p>
		</div>

		<a href="<?php echo dirname($_SERVER["REQUEST_URI"]); ?>"><< Voltar</a>
	</section>
</body>
</html>